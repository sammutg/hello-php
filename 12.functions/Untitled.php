<?php
	
// PART 1

//	function say_hello($name = 'toto') {
//		return "Hi there $name !";
//	}
	
//	$greeting = say_hello('Joe');
//	echo $greeting;
//	
//	echo say_hello('Joe');
//	echo say_hello('Joe');

//function pp($value) {
//	echo '<pre>';
//	print_r($value);
//	echo '</pre>';
//}
//
//$arr = array('name' => 'Joe', 'age' => 40, 'occupation' => 'teacher');
////print_r($arr);
//
//pp($arr);


// PART 2

//function array_pluck($toPluck, $arr) {
//	$ret = array();
//
//	foreach ($arr as $item) {
//		$ret[] = $item[$toPluck];
//	}
//
//	print_r($ret);
//
//}
//
//$people = array(
//	array('name' => 'Jeffrey', 'age' => 27, 'occupation' => 'Web Developer'),
//	array('name' => 'Joe', 'age' => 50, 'occupation' => 'Teacher'),
//	array('name' => 'Jane', 'age' => 30, 'occupation' => 'Marketing')
//);
//
//
//$plucked = array_pluck('occupation', $people);
//echo $plucked; 


// PART 3


function array_pluck($toPluck, $arr) {
	
	return array_map( function($item) use($toPluck) {

		return $item[$toPluck];

	}, $arr);

}

$people = array(
	array('name' => 'Jeffrey', 'age' => 27, 'occupation' => 'Web Developer'),
	array('name' => 'Joe', 'age' => 50, 'occupation' => 'Teacher'),
	array('name' => 'Jane', 'age' => 30, 'occupation' => 'Marketing')
);


$plucked = array_pluck('name', $people);
print_r($plucked);






