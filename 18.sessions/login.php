<?php 

session_start();

include("config.php");
include("functions.php");

	if ( $_SERVER['REQUEST_METHOD' ]  == 'POST' ) {

		$username = $_POST['username'];
		$password = $_POST['password'];

		if ( validate_user_credentials( $username, $password) ) {

			$_SESSION['username'] = $username;
			$_SESSION['password'] = $password;
			header("location:admin.php");
			die();

		} else {

			$message  = "Your login is not correct";

		}

	}
 
 ?>
<html>
<head>
	<title></title>
</head>
<body>

	<h1>Login</h1>
	<form action="login.php" method="post">
		<ul>
			<li>
				<label for="username">Username: </label>
				<input type="text" name="username">
			</li>

			<li>
				<label for="password">Password: </label>
				<input type="password" name="password">
			</li>

			<li>
				<input type="submit" value="Login" name="loginForm">	
			</li>			
			<?php 
				if ( isset( $message ) ) {
					echo "<li>" . $message . "</li>";
			}
			?>
		</ul>

	</form>

</body>
</html>