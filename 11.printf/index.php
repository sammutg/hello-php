<?php

// printf(string format [, mixed args [, mixed ...]])
// sprintf(string format [, mixed args [, mixed ...]])
// sscanf(string str, string format [, mixed &...])

	$name = "Max";
	$age = "34";
	
	echo "My name is $name, I am $age";

	//printf( 'My name is %s, I am %d', $name, $age );

	//$greeting = sprintf( 'My name is %s, I am %d', $name, $age );
	//echo( $greeting );

	// printf("Today is %s, %s %d", 'June', '7th', '2012');

	//$posted = sprintf("Today is %s, %s %d", 'June', '7th', '2012');
	//echo $posted;

	//$results = sscanf("June 7th, 2012", "%s %[^,], %d");
	//print_r( $results ); 

	//list( $month, $day, $year) = $results = sscanf("June 7th, 2012", "%s %[^,], %d");
	//echo $month;

	//list( $name, $age) = $infos = array( "Max", 27);
	//echo( "My name is $name, I am $age");
	
	// $results = sscanf("June 7th, 2012", "%s %[^,], %d", $month, $day, $year);
	// echo( "$month, $day, $year");
	
